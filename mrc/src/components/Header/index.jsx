import React from "react";
import PropTypes from 'prop-types';



export const Header = ({ title = "Header title" }) => {
  return <div>{title}</div>;
};


Header.propTypes = {
  title: PropTypes.string,
};

