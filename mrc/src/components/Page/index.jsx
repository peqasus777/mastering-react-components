import React from "react";
import { Header } from "../Header";
import { Main } from "../Main";
import { Footer } from "../Footer";

export const Page = () => {
  return (
    <>
      <Header title = {"Header"}/>
      <Main />
      <Footer title = {"Footer"} />
    </>
  );
};
