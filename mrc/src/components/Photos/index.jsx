import React from "react";
import "./photos.css";

export const Photos = ({ data }) => {
  return (
    <>
      <ul className="photos">
        {data.map((photo) => (
          <li key={photo.id}>
            <p>{photo.title}</p>
            <img src={photo.url} alt="" />
          </li>
        ))}
      </ul>
    </>
  );
};
