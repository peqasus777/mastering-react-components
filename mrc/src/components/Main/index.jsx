import React from 'react'
import { Photos } from '../Photos'
import {data} from '../../mock/data.js'

export const Main = () => {
  return (
    <>
        <Photos data = {data}/>
    </>
  )
}
